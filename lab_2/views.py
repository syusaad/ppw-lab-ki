from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

landing_page_content = 'My name is Syuhadah Binti Muhamed Saad. I come from University of Malaya and currently doing Student Exchange Program in Universitas Indonesia, studying computer science in FASILKOM, UI.'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'base2.html', response)