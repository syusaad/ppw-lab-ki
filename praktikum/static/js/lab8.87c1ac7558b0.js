// FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '151448042154903',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });

  // implement a function that check login status (getLoginStatus)
  // and run render function below, with a boolean true as a paramater if
  // login status has been connected
  FB.AppEvents.logPageView();

FB.getLoginStatus(function(response) {
      console.log("status : "+ response['status']);
      render(response['status']==='connected');
    });
  };

  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {
        return;
     }
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
  const render = loginFlag => {
    if (loginFlag) {
      // Jika yang akan dirender adalah tampilan sudah login

      // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
      getUserData(user => {
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        $('#lab8').html(
                        '<div class="container profile">' +
                            '<img class="cover" src="' + user.cover.source + '" alt="cover"/>' +
                            '<div class="data">' +
                                '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
                                '<h1>' + user.name + '</h1>' +
                                '<h2>' + user.about + '</h2>' +
                                '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
                            '</div>' +
                        '</div>' +
                        '<div class="container">' +
                            '<div class="row">' +
                                '<div class="col-xs-12">' +
                                    '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
                                '</div>' +
                            '</div>' +
                            '<div class="row">' +
                                '<div class="col-xs-12 buttons">' +
                                    '<button class="postStatus" onclick="postStatus()">Post</button>' +
                                    '<button class="logout" onclick="facebookLogout()">Logout</button>' +
                                '</div>' +
                            '</div>' +

                        '</div>'
                    );

        // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        $('#lab8').append("<div class='container' id='feeds'></div>");

        getUserFeed(feed => {
            feed.data.map(value => {
                // Render feed, kustomisasi sesuai kebutuhan.
                let id  = "'" + value.id + "'";
                if (value.message && value.story) {
                    $('#feeds').append(
                        '<div class="feed">' +
                        '<h1>' + value.message + '</h1>' +
                        '<h2>' + value.story + '</h2>' +
                        '<button class="deletePost" onclick="deleteFeed(' + id + ')">Delete</button>' +
                        '</div>'
                    );
                } else if (value.message) {
                    $('#feeds').append(
                        '<div class="feed">' +
                        '<h1>' + value.message + '</h1>' +
                        '<button class="deletePost" onclick="deleteFeed(' + id + ')">Delete</button>' +
                        '</div>'
                    );
                } else if (value.story) {
                    $('#feeds').append(
                        '<div class="feed">' +
                        '<h2>' + value.story + '</h2>' +
                        '<button class="deletePost" onclick="deleteFeed(' + id + ')">Delete</button>' +
                        '</div>'
                    );
                }
            });
        });
      });
    } else {
      // Tampilan ketika belum login
      $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
    }
  };

  const facebookLogin = () => {
	FB.login(function(response){
		console.log(response);
		FB.api('/me/', {fields: 'name,email,picture.width(150).height(10)'}, function(response){
			console.log('Info: '+response.name+' '+response.email);
		});
		render(true);
	}, {scope:'public_profile,user_posts,publish_actions,user_about_me,email', auth_type:'rerequest'});
};


  const facebookLogout = () => {
	FB.getLoginStatus(function(response){
		if (response.status === 'connected'){
			FB.logout();
			console.log("Logout successful!");
			render(false);
		}
	})
};


  // TODO: Lengkapi Method Ini
  // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?
const getUserData = (fun) => {
	FB.getLoginStatus(function(response){
		if (response.status === 'connected'){
			FB.api('/me?fields=id,name,about,cover,email,picture,gender', 'GET', function(response){
				console.log(response);
				if (response && !response.error){
					//picture = response.picture.data.url;
					//name = response.name;
					//userID = response.authResponse.userID || response.authResponse.userId;
					fun(response);
				}
				else {
					alert("Error getting User Data!");
				}
			});
		}
	});
};


  const getUserFeed = (fun) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
    FB.api(
        "/me/feed",
        function (response) {
          if (response && !response.error) {
            fun(response);
          }
        }
    );
  };

  const postFeed = (message) => {
    FB.api(
        "/me/feed",
        "POST",
        {
            "message": message
        },
        function (response) {
          console.log(response);
          if (response && !response.error) {
            location.reload();
          }
        }
    );
  };

  const deleteFeed = (id) => {
    FB.api(
    id,
    "DELETE",
    function (response) {
      if (response && !response.error) {
        location.reload();
      }
    }
);
  }

  const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
  };
