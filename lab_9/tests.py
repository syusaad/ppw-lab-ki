from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index

class lab9UnitTest(TestCase):
    def test_lab_9_exist(self):
        response = Client().get('/lab-9/')
        self.assertEqual(response.status_code,200)

    def test_lab_9_using_index_func(self):
        found = resolve('/lab-9/')
        self.assertEqual(found.func, index)


